jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});

jQuery.validator.addMethod("validaSenha",
        function(value, element, param) {
            if (this.optional(element)) {
                return true;
            } else if (!/[0-9]/.test(value)) {
                return false;
            } else if (!/[a-z]/.test(value)) {
                return false;
            }
                return true;
        },"A senha precisa ter pelo menos um numero e uma letra.");

$.validator.addMethod("alpha", function(value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z ]*$/);
},"O nome não pode conter numero ou caracteres.");

$( "#form_insere" ).validate({
  rules: {
    nome: {
      required: true,
      alpha:true
    },
    email:{
      required: true,
      email: true

    },
    senha:{
      minlength: 6,
      validaSenha: true
    }
  },

  messages:{
  nome:{
    required:"Esse campo não pode estar vazio.",
    number:"Nao pode number."
  },
  email:{
    required:"Esse campo não pode estar vazio.",
    email:"Digite um e-mail valido."
  }

  }

});
